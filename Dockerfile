FROM node:14-alpine
LABEL maintainer="@k33g_org"
LABEL authors="@k33g_org"
LABEL version="1.0"

COPY analyze.js /usr/local/bin/analyze

RUN chmod +x /usr/local/bin/analyze

CMD ["/bin/sh"]
