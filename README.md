# Packaging Mushrooms Linter

## Usage

```yaml
# === MushroomsLint ===
stages:
  - 🔎code-quality

include:
  - project: 'make-ci-lovable/code-quality/mushroomslint'
    file: 'mushrooms.lint.gitlab-ci.yml'

🔎:code:quality:mushrooms:lint:
  stage: 🔎code-quality
  extends: .mushrooms.lint:analyzer
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_MERGE_REQUEST_IID

```
